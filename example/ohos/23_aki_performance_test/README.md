## Example 23：*performance_test*

环境依赖：

* IDE：DevEco Studio 5.0.3.700

* SDK：5.0.0.60

#### **特别注意**：**因性能测试中多数为100000次测试且aki在debug模式下LOG信息较多，在运行前需要使用release模式编译**

### 1.依赖配置

- 本例使用源码依赖

  ```bash
  cd entry/src/main/cpp
  git clone
  ```

- CMakeLists.txt 配置依赖
  ```cmake
  add_subdirectory(aki)
  target_link_libraries(entry PUBLIC aki_jsbind)
  ```

### 2.用例说明

- 对aki以及napi原生接口进行耗时对比测试，测试结果可能存在小范围浮动

### 3.测试结果(函数运行100000次取平均数)

| module          | api content          | napi调用耗时（us） | aki调用耗时（us） | 耗时差距（us） |
| --------------- | -------------------- | ------------------ | ----------------- | -------------- |
| ArrayBuffer     | constructor          | 0.503              | 0.858             | 0.355          |
| globle function | void(*)()            | 0.874              | 0.921             | 0.047          |
| class           | constructor          | 2.302              | 4.339             | 2.037          |
| class           | void(*)(std::string) | 1.043              | 1.182             | 0.139          |
| class           | std::string(*)()     | 1.24               | 1.317             | 0.077          |
| class           | setter               | 1.047              | 1.14              | 0.093          |
| class           | getter               | 1.238              | 1.306             | 0.068          |
| aki::value      | IsNull               | 0.014              | 0.024             | 0.01           |
| aki::value      | IsBool               | 0.015              | 0.022             | 0.007          |
| aki::value      | IsNumber             | 0.006              | 0.013             | 0.007          |
| aki::value      | IsString             | 0.008              | 0.015             | 0.007          |
| aki::value      | IsObject             | 0.02               | 0.027             | 0.007          |
| aki::value      | IsObjectArray        | 0.009              | 0.015             | 0.006          |
| aki::value      | IsFunction           | 0.013              | 0.02              | 0.007          |
| aki::value      | IsUndefinded         | 0.019              | 0.025             | 0.006          |
| aki::value      | Set                  | 0.112              | 0.278             | 0.166          |
| aki::value      | NewObject            | 0.213              | 0.556             | 0.343          |
| aki::value      | CallMethod           | 1.156              | 2.276             | 1.12           |
| aki::value      | AsBool               | 0.005              | 0.005             | 差距小于1ns    |
| aki::value      | AsUnit8              | 0.005              | 0.006             | 0.001          |
| aki::value      | AsInt8               | 0.005              | 0.006             | 0.001          |
| aki::value      | AsUint16             | 0.006              | 0.006             | 差距小于1ns    |
| aki::value      | AsInt16              | 0.005              | 0.005             | 差距小于1ns    |
| aki::value      | AsInt                | 0.005              | 0.005             | 差距小于1ns    |
| aki::value      | AsInt64              | 0.005              | 0.006             | 0.001          |
| aki::value      | AsFloat              | 0.005              | 0.006             | 0.001          |
| aki::value      | AsDouble             | 0.005              | 0.005             | 差距小于1ns    |
| aki::value      | AsString             | 0.101              | 0.103             | 0.002          |
| aki::value      | GetHandle            | 0.005              | 0.012             | 0.007          |

