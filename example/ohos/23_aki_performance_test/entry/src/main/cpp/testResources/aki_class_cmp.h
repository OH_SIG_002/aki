/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AKI_CLASS_CMP_H
#define AKI_CLASS_CMP_H

#include "hilog/log.h"
#include "aki/jsbind.h"

namespace AKITEST {
class NapiClassTest {
public:
    NapiClassTest() = default;
    ~NapiClassTest() = default;

    static napi_value Init(napi_env env, napi_value exports);
    static napi_value Constructor(napi_env env, napi_callback_info info);
    static void Destructor(napi_env env, void *nativeObject, void *finalize);
    static napi_value SetMsg(napi_env env, napi_callback_info info);
    static napi_value GetMsg(napi_env env, napi_callback_info info);
    std::string testMsg;
};


class AkiClassTest {
public:
    AkiClassTest() = default;
    ~AkiClassTest() = default;
    void SetMsg(std::string msg);
    std::string GetMsg(void);
    std::string testMsg;
};

}

#endif //AKI_CLASS_CMP_H
