/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstring>
#include "hilog/log.h"
#include "napi/native_api.h"
#include "aki/jsbind.h"
#include "testManager/aki_test_time.h"
#include "testManager/aki_test_manager.h"
#include "testResources/akivalue_set_cmp.h"

namespace AKITEST {

#define TEST_COUNT 100000

void NapiSetProperty(napi_env env, napi_value obj, napi_value value)
{
    napi_status status = napi_ok;
    status = napi_set_named_property(env, obj, "name", value);
    if (status != napi_ok) {
        OH_LOG_Print(LOG_APP, LOG_ERROR, LOG_DOMAIN, "NapiSetProperty", "NapiSetProperty failed");
    }
}

void AkiValueSetCmp::NapiValueSetCheckTest()
{
    napi_value obj = nullptr;
    napi_value testString = nullptr;
    napi_env env = AKITEST::AkiTestManager::GetInstance().GetNapiEnv();
    const char *testStringarr = "Test";
    napi_create_object(env, &obj);
    napi_create_string_utf8(env, testStringarr, strlen(testStringarr), &testString);
    AKITEST::TestTime().NapiTestTime(TEST_COUNT, "Value", "SetProperty", NapiSetProperty, env, obj, testString);
}

void AkiSetProperty(aki::Value &value)
{
    value.Set("name", "Test");
}

void AkiValueSetCmp::AkiValueSetCheckTest()
{
    aki::Value val = aki::Value::NewObject();
    AKITEST::TestTime().AkiTestTime(TEST_COUNT, "Value", "SetProperty", AkiSetProperty, val);
}

}