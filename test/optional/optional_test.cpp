/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <optional>
#include "napi/native_api.h"
#include "aki/jsbind.h"

namespace {
#ifndef USING_CXX_STANDARD_11
std::optional<int> ReturnOptionalInt(bool isEmpty)
{
    if (isEmpty) {
        return {};
    }
    constexpr int testNum = 255;
    return std::optional<int>(testNum);
}

std::optional<std::string> ReturnOptionalString(bool isEmpty)
{
    if (isEmpty) {
        return {};
    }
    
    return std::optional<std::string>("hello aki");
}

bool PassingOptionalString(std::optional<std::string> opt)
{
    if (opt.has_value() && opt.value() == "Test String") {
        return true;
    }
    
    return false;
}

bool GetOptionalStringEmpty(std::optional<std::string> opt)
{
    if (opt.has_value())
        return false;
    
    return true;
}

struct Human {
public:
    Human(std::string name) : name_(name) {}

    std::string GetName() const { return name_; }
private:
    std::string name_;
};


bool PassingOptionalObject(std::optional<Human> opt)
{
    if (opt.has_value()) {
        if (opt.value().GetName() == "zhang san")
            return true;
    }
    
    return false;
}

bool GetOptionalObjectEmpty(std::optional<Human> opt)
{
    if (opt.has_value())
        return false;
    
    return true;
}

std::optional<Human> ReturnOptionalObject(bool isEmpty)
{
    if (isEmpty) {
        return {};
    }
    
    return std::optional<Human>(Human("Ben"));
}


std::optional<aki::Value> ReturnOtionalAkiValue(bool isEmpty)
{
    if (isEmpty) {
        return {};
    }
    
    return std::optional<aki::Value>(aki::Value("aki value string"));
}

bool GetOptionalAkiValue(std::optional<aki::Value> opt)
{
    if (opt.has_value()) {
        if (opt.value().As<std::string>() == "aki value js") {
            return true;
        }
    }
    
    return false;
}

bool GetOptionalAkiValueEmpty(std::optional<aki::Value> opt)
{
    if (opt.has_value()) {
        return false;
    }
    
    return true;
}

JSBIND_CLASS(Human) {
    JSBIND_CONSTRUCTOR<std::string>();
    JSBIND_METHOD(GetName);
}


JSBIND_GLOBAL() {
    JSBIND_FUNCTION(ReturnOptionalInt);
    JSBIND_FUNCTION(PassingOptionalString);
    JSBIND_FUNCTION(GetOptionalStringEmpty);
    JSBIND_FUNCTION(ReturnOptionalString);
    
    JSBIND_FUNCTION(PassingOptionalObject);
    JSBIND_FUNCTION(GetOptionalObjectEmpty);
    JSBIND_FUNCTION(ReturnOptionalObject);
    
    JSBIND_FUNCTION(ReturnOtionalAkiValue);
    JSBIND_FUNCTION(GetOptionalAkiValue);
    JSBIND_FUNCTION(GetOptionalAkiValueEmpty);
}
#endif

} // end of namespace